shader_type spatial;
render_mode unshaded, cull_disabled;


uniform sampler2D tex;
uniform vec4 color1 : hint_color;
uniform vec4 color2 : hint_color;
uniform float displ_amnt = 0.8;
uniform float displ_speed = 20.0;
uniform float pi = 3.14159265359;