extends KinematicBody


var velocity = Vector2(40, 40)


func _physics_process(delta):
	velocity = move_and_slide(velocity)
