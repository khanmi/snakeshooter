extends KinematicBody


var velocity = Vector3(40, 0, 40)
var gravity = -150
var speed = 3

func _ready():
	randomize()
	velocity = Vector3(rand_range(-speed, speed), 0, rand_range(-speed, speed))


func _physics_process(delta):
	if velocity.y > 0.5:
		velocity.y += gravity * delta
		
	velocity = move_and_slide(velocity)


func _on_Timer_timeout():
	randomize()
	velocity = Vector3(rand_range(-speed, speed), 0, rand_range(-speed, speed))
