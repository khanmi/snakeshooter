extends KinematicBody


export (float) var pistol_fire_rate = 0.3
export (float) var smg_fire_rate = 0.1

var small_bullet = preload("res://Source/Bullets/Small/SmallBullet.tscn")

onready var move_stick = get_node("HUD/move/joystick_button")
onready var rotate_stick = get_node("HUD/rotate/joystick_button")
onready var camera = $Pivot/Camera
onready var pistol_muzzle = $Pivot/Pistol/PistolMuzzle
onready var smg_muzzle = $Pivot/SMG/SMGMuzzle
onready var anim_player = $anim_player

var gravity = 20
var jump_speed = 8
var max_speed = 6
var mouse_sensitivity = 0.002

var velocity = Vector3()
var jump = false
var weapons = [
	'Knife',
	'Pistol',
	'SMG'
]
var damage = 30

signal shoot
signal dead

var current_weapon = "Knife"

var can_pistol_shoot = true
var can_smg_shoot = true


func _ready():
	if OS.get_name() == "Android":
		toggle_ui(true)
	else:
		toggle_ui(false)
	$PistolTimer.wait_time = pistol_fire_rate
	$SMGTimer.wait_time = smg_fire_rate


func toggle_ui(value):
	$HUD/jump.visible = value
	$HUD/attack.visible = value
	$HUD/move.visible = value
	$HUD/rotate.visible = value


func toggle_weapons(selected_weapon):
	for weapon in $Pivot.get_children():
		if weapon.name == "Camera":
			continue
		weapon.visible = false
		get_node("HUD/Weapons/" + weapon.name).modulate = Color(0, 0, 0, 1)
	$Pivot.get_node(selected_weapon).visible = true
	get_node("HUD/Weapons/" + selected_weapon).modulate = Color(1, 1, 1, 1)


func get_input():
	jump = false
	if Input.is_action_just_pressed("jump"):
		jump = true
	for weapon in weapons:
		if Input.is_action_just_pressed(weapon):
			toggle_weapons(weapon)
			current_weapon = weapon
	var input_dir = Vector3()
	var move_value = move_stick.get_value()
	var move_vec = Vector3(move_value.x, 0, move_value.y)
	input_dir += move_vec
	if Input.is_action_pressed("move_forward"):
		input_dir += -camera.global_transform.basis.z
	if Input.is_action_pressed("move_back"):
		input_dir += camera.global_transform.basis.z
	if Input.is_action_pressed("move_left"):
		input_dir += -camera.global_transform.basis.x
	if Input.is_action_pressed("move_right"):
		input_dir += camera.global_transform.basis.x
	input_dir = input_dir.normalized()
	var rotate_value = rotate_stick.get_value()
	rotate_y(-rotate_value.x * mouse_sensitivity * 10)
	$Pivot.rotate_x(-rotate_value.y * mouse_sensitivity * 10)
	$Pivot.rotation.x = clamp($Pivot.rotation.x, -1.2, 1.2)
	return input_dir


func _unhandled_input(event):
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	if event is InputEventMouseMotion:
		rotate_y(-event.relative.x * mouse_sensitivity)
		$Pivot.rotate_x(-event.relative.y * mouse_sensitivity)
		$Pivot.rotation.x = clamp($Pivot.rotation.x, -1.2, 1.2)


func handle_input():
	if Input.is_action_pressed("shoot"):
		_on_attack_button_down()


func _physics_process(delta):
	check_attack()
	handle_input()
	velocity.y -= gravity * delta
	var desired_velocity = get_input() * max_speed
	
	velocity.x = desired_velocity.x
	velocity.z = desired_velocity.z
	
	velocity = move_and_slide(velocity, Vector3.UP, true)
	
	if jump and is_on_floor():
		velocity.y = jump_speed


func _on_PistolTimer_timeout():
	can_pistol_shoot = true


func _on_SMGTimer_timeout():
	can_smg_shoot = true


func hurt(damage):
	$HUD/HPBox/HPBar.value -= damage


func died():
	hide()
	$Body.disabled = true
	$Feet.disabled = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	set_physics_process(false)
	set_process(false)
	emit_signal("dead")


func _on_HPBar_value_changed(value):
	if value <= 0:
		died()


func check_attack():
	for body in $KnifeAttack.get_overlapping_bodies():
		if body.is_in_group("snakes"):
			if anim_player.current_animation_position > 0.2 and anim_player.current_animation_position < 0.21:
				body.hurt(damage)


func _on_Knife_pressed():
	toggle_weapons("Knife")


func _on_Pistol_pressed():
	toggle_weapons("Pistol")


func _on_Rifle_pressed():
	toggle_weapons("SMG")


func _on_jump_pressed():
	if is_on_floor():
		velocity.y = jump_speed


func _on_attack_button_down():
	if current_weapon == "Knife":
		anim_player.play("Knife")
	elif current_weapon == "Pistol" and can_pistol_shoot:
		anim_player.play(current_weapon)
		emit_signal("shoot", small_bullet, pistol_muzzle.global_transform)
		can_pistol_shoot = false
		$PistolTimer.start()
	elif current_weapon == "SMG" and can_smg_shoot:
		anim_player.play(current_weapon)
		emit_signal("shoot", small_bullet, smg_muzzle.global_transform)
		can_smg_shoot = false
		$SMGTimer.start()


func _on_home_pressed():
	get_tree().change_scene("res://Source/Main/Main.tscn")
