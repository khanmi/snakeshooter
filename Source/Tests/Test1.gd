extends Spatial

var snakes_killed = 0

func _ready():
	hide_menu()
	connect_snakes()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func connect_snakes():
	for snake in $Snakes.get_children():
		snake.connect("dead", self, "on_snake_death")

func on_snake_death():
	snakes_killed += 1
	if snakes_killed >= 4:
		$HUD/Menu/Panel/Header.text = "You Win"
		$Wamiq.died()
		$HUD/Menu.show()
	$HUD/SnakeBox/Amount/Killed.text = str(snakes_killed)


func hide_menu():
	$HUD/Menu.visible = false


func show_menu():
	$HUD/Menu.visible = true


func _input(event):
	if $HUD/Menu.visible:
		return
	if event.is_action_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		elif Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)



func _on_Wamiq_shoot(bullet, xform):
	var b = bullet.instance()
	b.start(xform)
	add_child(b)


func _on_Wamiq_dead():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	show_menu()


func _on_quit_button_pressed():
	get_tree().quit()


func _on_play_button_pressed():
	get_tree().change_scene(filename)


func _on_Music_finished():
	$Music.play()
