extends Area

var speed = 15
var velocity = Vector3()
var damage = 10


func start(xform):
	transform = xform
	velocity = -transform.basis.z * speed


func _process(delta):
	transform.origin += velocity * delta


func _on_Timer_timeout():
	queue_free()


func _on_SmallBullet_body_entered(body):
	if body is StaticBody:
		queue_free()
	if body.is_in_group('snakes'):
		print("hit snake")
		body.hurt(damage)
		queue_free()
