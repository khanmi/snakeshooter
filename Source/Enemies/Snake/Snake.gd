extends KinematicBody


var motion = Vector3.ZERO
var gravity = -10
var damage = 10
var hurt = false
signal dead


func _ready():
	$AnimationPlayer.play("SnakeArmature|Snake_Idle")


func _physics_process(delta):
	if not hurt:
		motion = Vector3.ZERO
	detect_player()
	attack_player()
	motion.y += gravity * delta
	motion = move_and_slide(motion, Vector3.UP)


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "SnakeArmature|Snake_Walk":
		$AnimationPlayer.play("SnakeArmature|Snake_Walk")
		return
	$AnimationPlayer.play("SnakeArmature|Snake_Idle")


func detect_player():
	if hurt:
		return
	for body in $DetectAttackArea.get_overlapping_bodies():
		if body.name == "Wamiq":
			rotation = body.rotation
			$AnimationPlayer.play("SnakeArmature|Snake_Attack")
			return
	var walk = false
	for body in $DetectArea.get_overlapping_bodies():
		if body.name == "Wamiq":
			follow(body)
			$AnimationPlayer.play("SnakeArmature|Snake_Walk")
			walk = true
	if not walk:
		$AnimationPlayer.play("SnakeArmature|Snake_Idle")


func attack_player():
	if hurt:
		return
	for body in $AttackArea.get_overlapping_bodies():
		if body.name == "Wamiq":
			if $AnimationPlayer.current_animation_position > 0.45 and $AnimationPlayer.current_animation_position < 0.46:
				body.hurt(damage)


func follow(body):
	rotation = body.rotation
	var dx = body.transform.origin.x - transform.origin.x
	var dz = body.transform.origin.z - transform.origin.z
	dx = dx
	dz = dz
	motion = Vector3(dx, 0, dz)


func hurt(damage):
	hurt = true
	motion = -motion
	$AnimationPlayer.play("SnakeArmature|Snake_Idle")
	$hurt_timer.start()
	$HPBox/Viewport/HPBar.value -= damage


func _on_HPBar_value_changed(value):
	if value <= 0:
		die()


func die():
	emit_signal("dead")
	queue_free()


func pause():
	set_physics_process(false)
	set_process(false)


func _on_hurt_timer_timeout():
	hurt = false
